package bysun.weather.tools;

import bysun.weather.Commons;
import bysun.weather.http.HTTPRequester;
import opensource.jpinyin.PinyinFormat;
import opensource.jpinyin.PinyinHelper;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * 构建 地点信息本地缓存
 */
public class LocationInfoBuilder {
    private JsonFactory factory;
    private ObjectMapper mapper;
    private final String root = Commons.get("location.level1.url", Commons.LEAVE_1);
    private final String level2 = Commons.get("location.level2.url", Commons.LEAVE_2);
    private final String level3 = Commons.get("location.level3.url", Commons.LEAVE_3);

    public LocationInfoBuilder() {
        factory = new JsonFactory();
        mapper = new ObjectMapper(factory);
    }

    /**
     * 构建地点信息本地缓存文件
     */
    public void build() {
        try {
            List<Location> data = load();
            File file = new File(Commons.get("location.file.name", Commons.LOCA_FILE_NAME));
            if(null!=file.getParentFile()){
                file.getParentFile().mkdirs();
            }

            mapper.writeValue(file, data);
        } catch (IOException e) {
        }
    }

    private List<Location> load() throws IOException {
        List<Location> result;
        Map<String, String> params = new TreeMap<String, String>();

        Map<String, String> rootData = jsonToMap(loadJson(root));
        result = new ArrayList<Location>();
        for (String key : rootData.keySet()) {
            // 请求2级
            load(params, key, result);
        }
        return result;
    }

    private void load(Map<String, String> params, String code, List<Location> result) throws IOException {
        params.clear();
        params.put("code", code);
        Map<String, String> levelTwo = jsonToMap(loadJson(level2, params));
        for (String key : levelTwo.keySet()) {
            // 请求三级
            params.clear();
            params.put("code", code+key);
            Map<String, String> levelThree = jsonToMap(loadJson(level3, params));
            for (Map.Entry<String, String> entry : levelThree.entrySet()) {
                Location loca = new Location();
                loca.setName(entry.getValue());
                loca.setCode(code + key + entry.getKey());
                loca.setPinyinName(PinyinHelper.convertToPinyinString(entry.getValue(), "", PinyinFormat.WITHOUT_TONE));
                result.add(loca);
            }
        }
    }

    private Map<String, String> jsonToMap(String json) throws IOException {
        return mapper.readValue(json,HashMap.class);
    }

    private String loadJson(String s, Map<String, String> params) {
        String url = s;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            String val = entry.getValue();

            url = url.replaceAll("(.*?)\\$\\{"+key+"\\}(.*)", "$1"+val+"$2");
        }
        return loadJson(url);
    }

    private String loadJson(String s) {
        return HTTPRequester.get(s);
    }


    public static void main(String[] args) {
        LocationInfoBuilder builder = new LocationInfoBuilder();
        builder.build();
    }
}
