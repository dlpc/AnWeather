package bysun.weather;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.way.anweather.App;

/**
 * User: Bysun
 * Date: 13-7-3
 * Time: 下午2:54
 */
public class Commons {
    public final static String LOCA_FILE_NAME="locations.json";

    public final static int CONN_TOUT = 5000;
    public final static int READ_TOUT = 5000;
    public final static int MAX_CONNS = 10;
    public final static int MAX_R_CONNS = 5;

    public final static String LEAVE_1 = "http://www.weather.com.cn/data/city3jdata/china.html";
    public final static String LEAVE_2 = "http://www.weather.com.cn/data/city3jdata/provshi/${code}.html";
    public final static String LEAVE_3 = "http://www.weather.com.cn/data/city3jdata/station/${code}.html";

    public final static String URL_LAST_HOUR = "http://www.weather.com.cn/data/sk/${code}.html";
    public final static String URL_TODAY = "http://www.weather.com.cn/data/cityinfo/${code}.html";
    public final static String URL_SIX_DAY = "http://m.weather.com.cn/data/${code}.html";

    public final static Properties PROPERTIES ;

    static {
        PROPERTIES = new Properties();
        InputStream is = null;
        try {
//            is = Commons.class.getClassLoader().getResourceAsStream("weather.properties");
        	is = App.getInstance().getAssets().open("weather.properties"); 
            PROPERTIES.load(is);
        } catch (IOException e) {

        }finally {
            if(null != is)
                try { is.close(); } catch (IOException e) { }
        }
    }

    public static String get(String key,String def){
        return PROPERTIES.getProperty(key,def);
    }

    public static int get(String key,int def){
        return Integer.valueOf(PROPERTIES.getProperty(key,String.valueOf(def)));
    }

    public static boolean isNotBlank(Object obj){
        if(null == obj)
            return false;
        if(obj instanceof String){
            if("".equals(((String)obj).trim()))
                return false;
            else
                return true;
        }else
            return true;
    }
}
