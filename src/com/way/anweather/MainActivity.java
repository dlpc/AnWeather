package com.way.anweather;

import java.io.IOException;

import bysun.weather.WeatherLastHour;
import bysun.weather.WeatherReader;
import bysun.weather.WeatherSixDay;
import bysun.weather.WeatherToday;
import bysun.weather.tools.LocationInfoBuilder;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends Activity {
	private TextView mTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mTextView = (TextView) findViewById(R.id.info);
		new MyTask().execute();
	}

	class MyTask extends AsyncTask<Void, Void, String> {
		
		@Override
		protected String doInBackground(Void... params) {
			try {
//				LocationInfoBuilder builder = new LocationInfoBuilder();
//				builder.build();
				WeatherLastHour lastHour = WeatherReader
						.getLastHour("101280601");
				WeatherSixDay sixDay = WeatherReader.getSixDay("101280601");
				WeatherToday today = WeatherReader.getToday("101280601");
				Log.i("way", lastHour.toString());
				Log.i("way", sixDay.toString());
				Log.i("way", today.toString());
				return sixDay.toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (!TextUtils.isEmpty(result)) {
				mTextView.setText(result);
			}
		}

	}
}
